# IrrigaJá

O IrrigaJá é um projeto desenvolvido por um grupo de alunos cursando a matéria Projeto Integrador 2, juntando os conceitos e práticas da engenharia aeroespacial, engenharia automotiva, engenharia eletrônica, engenharia de energia e engenharia de software.

O IrrigaJá tem como objetivo a criação de um sistema de irrigação automatizado conectado a uma estação meteorológica para auxiliar a agricultura familiar.


## Equipe

<center>

<div style="display: flex; flex-direction: row; gap: 15px; flex-wrap: wrap; justify-content: center;" >
    <div>
        <a href="https://github.com/italovinicius18 ">
                <img style="border-radius: 50%;"         src="https://github.com/italovinicius18.png" width="100px;"/>
                <h5 class="text-center">Italo Vinicius</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/IanFPFerreira">
                <img style="border-radius: 50%;"         src="https://github.com/IanFPFerreira.png" width="100px;"/>
                <h5 class="text-center">Ian Fillipe</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/gabrielavelino">
                <img style="border-radius: 50%;"         src="https://github.com/gabrielavelino.png" width="100px;"/>
                <h5 class="text-center">Gabriel Avelino</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/guilhermemoraisr">
                <img style="border-radius: 50%;"         src="https://github.com/guilhermemoraisr.png" width="100px;"/>
                <h5 class="text-center">Guilherme de Morais</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/AlvaroLeles">
                <img style="border-radius: 50%;"         src="https://github.com/AlvaroLeles.png" width="100px;"/>
                <h5 class="text-center">Álvaro Leles</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/italofernandes13">
                <img style="border-radius: 50%;"         src="https://github.com/italofernandes13.png" width="100px;"/>
                <h5 class="text-center">Ítalo Fernandes</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/gabrielbpn">
                <img style="border-radius: 50%;"         src="https://github.com/gabrielbpn.png" width="100px;"/>
                <h5 class="text-center">Gabriel Bonifácio</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/otaviolg1">
                <img style="border-radius: 50%;"         src="https://github.com/otaviolg1.png" width="100px;"/>
                <h5 class="text-center">Otávio Leandro</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/lais.loureiro123">
                <img style="border-radius: 50%;"         src="https://github.com/lais.loureiro123.png" width="100px;"/>
                <h5 class="text-center">Lais Loureiro</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/hugopiores">
                <img style="border-radius: 50%;"         src="https://github.com/hugopiores.png" width="100px;"/>
                <h5 class="text-center">Hugo Procópio</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/">
                <img style="border-radius: 50%;"         src="https://github.com/" width="100px;"/>
                <h5 class="text-center">Victor Cavalcante</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/">
                <img style="border-radius: 50%;"         src="https://github.com/" width="100px;"/>
                <h5 class="text-center">Matheus Schappo</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/schapponi">
                <img style="border-radius: 50%;"         src="https://github.com/schapponi.png" width="100px;"/>
                <h5 class="text-center">Gustavo Silva</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/pedrinfonseca17">
                <img style="border-radius: 50%;"         src="https://github.com/pedrinfonseca17.png" width="100px;"/>
                <h5 class="text-center">Pedro Fonseca</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/victor-rayan">
                <img style="border-radius: 50%;"         src="https://github.com/victor-rayan.png" width="100px;"/>
                <h5 class="text-center">Victor Rayan</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/SamuelNoB">
                <img style="border-radius: 50%;"         src="https://github.com/SamuelNoB.png" width="100px;"/>
                <h5 class="text-center">Samuel Nogueira</h5>
        </a>
    </div>
    <div>
        <a href="https://github.com/gleal17">
                <img style="border-radius: 50%;"         src="https://github.com/gleal17.png" width="100px;"/>
                <h5 class="text-center">Guilherme Leal</h5>
        </a>
    </div>
</div>
    
</center>