# Irrigajá - Docs

[WIKI](https://fga-pi2.gitlab.io/2023-2/grupo-06/docs)

## Escopo Final

O Irrigajá é um sistema de irrigação acoplado a uma pequena estação meteorológica automatizado e conectado, especialmente concebido para atender às necessidades da agricultura familiar. O sistema é composto por um conjunto de dispositivos eletrônicos, sensores e uma interface de controle móvel. Os sensores coletam dados de umidade do solo, temperatura e velocidade do vento. Com base nos dados de umidade do solo, o sistema controlará a irrigação de forma automatizada, visando otimizar o uso da água e melhorar a produtividade das plantações. Além disso, ele possui uma aplicação que permite aos usuários monitorar em tempo real os dados dos sensores e controlar remotamente o sistema de irrigação.

## Post mortem

### Lições aprendidas


#### 1. Trabalho em Equipe e Coordenação
- A coordenação eficaz em uma equipe multidisciplinar é crucial.
- A importância de comunicar ideias clara e concisamente para manter a equipe alinhada.

#### 2. Desenvolvimento Full-Stack e Integração de Sistemas
- Compreensão da interação entre diferentes componentes de um sistema.
- Importância de um design cuidadoso para compatibilidade e eficiência.

#### 3. Adaptação e Solução de Problemas
- Capacidade de adaptar-se rapidamente e encontrar soluções criativas para desafios.

#### 4. Aplicação Prática de Conhecimentos Teóricos
- Aplicação prática de conhecimentos teóricos em um contexto real.
- Desde o dimensionamento de sistemas energéticos até a elaboração de softwares.

#### 5. Importância do Feedback e Revisão Contínua
- Revisão e ajustes com base no feedback são essenciais para o aprimoramento.
- Estar aberto a críticas construtivas é fundamental.

#### 6. Compromisso com a Sustentabilidade e Impacto Social
- Desenvolvimento de soluções tecnológicas com impacto social positivo.
- Foco na agricultura familiar e na otimização do uso da água.

#### 7. Gerenciamento de Tempo e Prioridades
- Aprender a gerenciar o tempo e priorizar tarefas.
- Entender como as tarefas individuais se encaixam no cronograma geral.

#### 8. Inovação e Criatividade
- Estímulo à inovação e criatividade no desenvolvimento de software e design.
- Pensar fora da caixa para superar desafios técnicos.

## Conclusão

O projeto Irrigajá foi uma jornada valiosa de aprendizado e crescimento, reforçando a importância da colaboração, inovação e comprometimento com a sustentabilidade.

